#include "Shape.h"
/*
Constructor for Shape object.
Input: shape name, shape type.
Output: none.
*/
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

/*
Function prints Shape details.
Input: none.
Output: none.
*/
void Shape::printDetails() const
{
	cout << this->_type << setw(SPACE) << this->_name << setw(SPACE) << this->getArea() << setw(SPACE) << this->getPerimeter() << endl;
}

/*
Function returns shape type.
Input: none.
Output: the shape's type.
*/
std::string Shape::getType() const
{
	return this->_type;
}

/*
Function returns the shape's name.
Input: none.
Output: the shape's name.
*/
std::string Shape::getName() const
{
	return this->_name;
}


