#include "Arrow.h"

/*
Constructor function for Arrow object.
Input: 2 points, type and name.
Output: none.
*/
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}

/*
Destructor function for Triangle object.
Input: none.
Output: none.
*/
Arrow::~Arrow()
{
	this->_points.clear();
}

/*
Function returns 0 for area of an arrow.
Input: none.
Output: 0.
*/
double Arrow::getArea() const
{
	return 0;
}

/*
Function calculates and returns the distance between
the two points of an arrow.
Input: none.
Output: distance between the two arrow points.
*/
double Arrow::getPerimeter() const
{
	return this->_points[FIRST_POINT].distance(this->_points[SECOND_POINT]);
}

/*
Function draws an arrow.
Input: canvas reference.
Output: none.
*/
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

/*
Function moves an arrow according to the moving scale given.
Input: point representing the moving scale.
Output: none.
*/
void Arrow::move(const Point& other)
{
	this->_points[FIRST_POINT] += other;
	this->_points[SECOND_POINT] += other;
}

/*
Function clears a drawing of a triangle.
Input: canvas reference.
Output: none.
*/
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


