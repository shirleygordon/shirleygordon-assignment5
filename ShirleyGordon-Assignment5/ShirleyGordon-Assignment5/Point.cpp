#include "Point.h"

/*
Constructor for Point object.
Input: x, y coordinates.
Output: none.
*/
Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

/*
Copy constructor for Point object.
Input: Point object reference.
Output: none.
*/
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

/*
Function adds the values of two points and returns
a new point containing the results.
Input: point reference.
Output: new point containing the results.
*/
Point Point::operator+(const Point& other) const
{
	Point newPoint(other);

	newPoint._x += this->_x;
	newPoint._y += this->_y;

	return newPoint;
}

/*
Function adds the values of the given point to the
current point and returns a reference to it.
Input: point reference.
Output: point reference containing the results.
*/
Point & Point::operator+=(const Point & other)
{
	*this = *this + other;

	return *this;
}

/*
Function returns point x coordinate.
Input: none.
Output: point x coordinate.
*/
double Point::getX() const
{
	return this->_x;
}

/*
Function returns point y coordinate.
Input: none.
Output: point y coordinate.
*/
double Point::getY() const
{
	return this->_y;
}

/*
Function returns distance between the current point to the given point.
Input: point reference.
Output: the distance between the two points.
*/
double Point::distance(const Point & other) const
{
	return sqrt(pow(this->_x - other._x, SQUARED) + pow(this->_y - other._y, SQUARED));
}
