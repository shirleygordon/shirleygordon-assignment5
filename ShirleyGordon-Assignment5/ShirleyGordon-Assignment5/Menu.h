#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include <vector>
#include <stdlib.h>
#include <string>

using std::cin;
using std::flush;
using std::vector;
using std::string;

enum mainMenu:int {ADD_NEW_SHAPE, MODIFY, DELETE_ALL, EXIT};
enum addNewShapeEnum:int {ADD_CIRCLE, ADD_ARROW, ADD_TRI, ADD_RECT};
enum modifyGetInfoEnum:int {MOVE, GET_DETAILS, REMOVE};

class Menu
{
private:
	Canvas _canvas;
	vector<Point> getPoints(unsigned int numOfPoints);
	void addCircle(vector<Shape*>& shapes);
	void addArrow(vector<Shape*>& shapes);
	void addTriangle(vector<Shape*>& shapes);
	void addRectangle(vector<Shape*>& shapes);
	int printShapes(const vector<Shape*> shapes);
	void clearAll(const vector<Shape*> shapes);
	void drawAll(const vector<Shape*> shapes);

public:

	Menu() : _canvas() {};
	~Menu() {};

	int mainMenu();
	void addNewShape(vector<Shape*>& shapes);
	void modifyGetInfo(vector<Shape*>& shapes);
	void deleteAll(vector<Shape*>& shapes);
};

