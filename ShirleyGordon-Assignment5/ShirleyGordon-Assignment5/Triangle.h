#pragma once
#include "Polygon.h"
#include <string>

#define HALF 2

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle() {};
	
	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
};