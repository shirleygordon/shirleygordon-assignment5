#include "Polygon.h"

/*
Destructor for Polygon object.
Input: none.
Output: none.
*/
Polygon::~Polygon()
{
	this->_points.clear();
}

/*
Function moves a polygon according to the moving scale given.
Input: point representing the moving scale.
Output: none.
*/
void Polygon::move(const Point& other)
{
	unsigned int i = 0;

	for (i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}


