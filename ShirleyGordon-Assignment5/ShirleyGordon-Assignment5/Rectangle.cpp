#include "Rectangle.h"

/*
Constructor function for Rectangle object.
Input: top left corner point, length, width, type and name.
Output: none.
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	int x = 0, y = 0;

	this->_points.push_back(a);

	// Calculate the bottom right point
	x = a.getX() + length;
	y = a.getY() + width;
	Point b(x, y);
	this->_points.push_back(b);

	// In order to handle negative length or width, use absolute value.
	this->_length = abs(length); 
	this->_width = abs(width);
}

/*
Function calculates and returns area of rectangle.
Input: none.
Output: rectangle area.
*/
double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}

/*
Function calculates and returns perimeter of rectangle.
Input: none.
Output: rectangle perimeter.
*/
double myShapes::Rectangle::getPerimeter() const
{
	return this->_width * TWICE + this->_length * TWICE;
}

/*
Function draws a rectangle.
Input: canvas reference.
Output: none.
*/
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

/*
Function clears a drawing of a rectangle.
Input: canvas reference.
Output: none.
*/
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


