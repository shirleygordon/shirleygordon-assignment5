#include "Circle.h"

/*
Constructor function for Circle object.
Input: center point, radius, type and name.
Output: none.
*/
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : _center(center), Shape(name, type)
{
	this->_radius = abs(radius); // In order to handle negative radius, use absolute value.
}

/*
Function returns the center of a circle.
Input: none.
Output: center of this circle.
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}

/*
Function returns the radius of a circle.
Input: none.
Output: radius of this circle.
*/
double Circle::getRadius() const
{
	return this->_radius;
}

/*
Function calculates and returns area of circle.
Input: none.
Output: circle area.
*/
double Circle::getArea() const
{
	return pow(_radius, SQUARED) * PI;
}

/*
Function calculates and returns perimeter of circle.
Input: none.
Output: circle perimeter.
*/
double Circle::getPerimeter() const
{
	return TWICE * PI * _radius;
}

/*
Function draws a circle.
Input: canvas reference.
Output: none.
*/
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

/*
Function moves a circle according to the moving scale given.
Input: point representing the moving scale.
Output: none.
*/
void Circle::move(const Point& other)
{
	this->_center += other;
}

/*
Function clears a drawing of a circle.
Input: canvas reference.
Output: none.
*/
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


