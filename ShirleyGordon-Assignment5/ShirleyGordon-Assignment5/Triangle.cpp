#include "Triangle.h"

/*
Constructor function for Triangle object.
Input: 3 points, type and name.
Output: none.
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

/*
Function calculates and returns area of triangle.
Input: none.
Output: triangle area.
*/
double Triangle::getArea() const
{
	return abs(_points[FIRST_POINT].getX()*(_points[SECOND_POINT].getY() - _points[THIRD_POINT].getY()) +
			   _points[SECOND_POINT].getX()*(_points[THIRD_POINT].getY() - _points[FIRST_POINT].getY()) +
			   _points[THIRD_POINT].getX()*(_points[FIRST_POINT].getY() - _points[SECOND_POINT].getY())) / HALF;
}

/*
Function calculates and returns perimeter of triangle.
Input: none.
Output: triangle perimeter.
*/
double Triangle::getPerimeter() const
{
	return this->_points[FIRST_POINT].distance(this->_points[SECOND_POINT]) + this->_points[SECOND_POINT].distance(this->_points[THIRD_POINT]) + this->_points[THIRD_POINT].distance(this->_points[FIRST_POINT]);
}

/*
Function draws a triangle.
Input: canvas reference.
Output: none.
*/
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

/*
Function clears a drawing of a triangle.
Input: canvas reference.
Output: none.
*/
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
