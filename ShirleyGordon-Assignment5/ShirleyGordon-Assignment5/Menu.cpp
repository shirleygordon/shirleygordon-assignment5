#include "Menu.h"

/*
Function creates a vector of points from user input.
Input: number of points.
Output: vector of points.
*/
vector<Point> Menu::getPoints(unsigned int numOfPoints)
{
	vector<Point> points;
	unsigned int i = 0;
	double x = 0, y = 0;

	for (i = 0; i < numOfPoints; i++)
	{
		// Get coordinates from user
		cout << "Enter the X of point number " << i + 1 << ":" << endl;
		cin >> x;
		cout << "Enter the Y of point number " << i + 1 << ":" << endl;
		cin >> y;

		Point p(x, y); // Create point
		points.push_back(p); // Push point into points vector
	}

	return points;
}

/*
Function adds a new circle to the shapes vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::addCircle(vector<Shape*>& shapes)
{
	double x = 0, y = 0, radius = 0;
	string name = "";

	// Get circle fields from user
	cout << "Please enter X:" << endl;
	cin >> x;
	cout << "Please enter Y:" << endl;
	cin >> y;
	cout << "Please enter radius:" << endl;
	cin >> radius;
	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Point center(x, y); // Create center point
	Shape* newCircle = new Circle(center, radius, "Circle", name); // Create new circle
	shapes.push_back(newCircle); // Add circle to vector
	shapes[shapes.size() - 1]->draw(this->_canvas); // Draw the circle
}

/*
Function adds a new arrow to the shapes vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::addArrow(vector<Shape*>& shapes)
{
	vector<Point> points;
	string name = "";
	const int ARROW_POINTS = 2;

	// Get 2 points from user
	points = getPoints(ARROW_POINTS);

	// Get shape name
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	// Create new arrow object and push it into the shapes vector
	Shape* newArrow = new Arrow(points[FIRST_POINT], points[SECOND_POINT], "Arrow", name);
	shapes.push_back(newArrow);

	shapes[shapes.size() - 1]->draw(_canvas); // Draw arrow
}

/*
Function adds a new triangle to the shapes vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::addTriangle(vector<Shape*>& shapes)
{
	vector<Point> points;
	string name = "";
	const int TRIANGLE_POINTS = 3;

	// Get 3 points from user
	points = getPoints(TRIANGLE_POINTS);

	// Get shape name
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	// Check if points are invalid
	// If the coordinates are all on the same axis, the points are invalid.
	if (!((points[FIRST_POINT].getX() != points[SECOND_POINT].getX()
		|| points[FIRST_POINT].getX() != points[THIRD_POINT].getX()
		|| points[SECOND_POINT].getX() != points[THIRD_POINT].getX())
		&& (points[FIRST_POINT].getY() != points[SECOND_POINT].getY()
		|| points[FIRST_POINT].getY() != points[THIRD_POINT].getY()
		|| points[SECOND_POINT].getY() != points[THIRD_POINT].getY())))
	{
		// Print error and exit the function (don't create new triangle)
		cerr << "The points entered create a line." << endl << flush;
		system("PAUSE");
		return;
	}

	// If the points are valid create new triangle object and push it into the shapes vector
	Shape* newTriangle = new Triangle(points[FIRST_POINT], points[SECOND_POINT], points[THIRD_POINT], "Triangle", name);
	shapes.push_back(newTriangle);

	shapes[shapes.size() - 1]->draw(_canvas); // Draw triangle
}

/*
Function adds a new rectangle to the shapes vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::addRectangle(vector<Shape*>& shapes)
{
	double x = 0, y = 0, length = 0, width = 0;
	string name = "";

	cout << "Enter the X of the to left corner:" << endl;
	cin >> x;
	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;
	cout << "Please enter the length of the shape:" << endl;
	cin >> length;
	cout << "Please enter the width of the shape:" << endl;
	cin >> width;
	cout << "Enter the name of the shape:" << endl;
	cin >> name;

	// The length or width can't be 0
	if (length == 0 || width == 0)
	{
		// Print error and exit the function (don't create new rectangle)
		cerr << "Length or Width can't be 0." << endl << flush;
		system("PAUSE");
		return;
	}

	Point corner(x, y); // Create corner point
	
	// Create new rectangle object and push it into the shapes vector
	Shape* newRectangle = new myShapes::Rectangle(corner, length, width, "Rectangle", name);
	shapes.push_back(newRectangle);

	shapes[shapes.size() - 1]->draw(_canvas); // Draw rectangle
}

/*
Function prints a list of all the shapes in the vector
and returns choice from user.
Input: shapes vector.
Output: number of shape the user chose.
*/
int Menu::printShapes(const vector<Shape*> shapes)
{
	unsigned int i = 0;
	int choice = 0;
	
	do
	{
		system("CLS"); // Clear the screen

		// Print list of shapes
		for (i = 0; i < shapes.size(); i++)
		{
			cout << "Enter " << i << " for " << shapes[i]->getName() << "(" << shapes[i]->getType() << ")" << endl;
		}

		cin >> choice; // Get choice
	} while (choice < 0 || choice >= shapes.size()); // Ask for input until it's valid

	return choice;
}

/*
Function clears all shapes from the canvas.
Input: shapes vector.
Output: none.
*/
void Menu::clearAll(const vector<Shape*> shapes)
{
	unsigned int i = 0;

	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->clearDraw(_canvas);
	}
}

/*
Function draws all shapes.
Input: shapes vector.
Output: none.
*/
void Menu::drawAll(const vector<Shape*> shapes)
{
	unsigned int i = 0;

	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->draw(_canvas);
	}
}

/*
Function prints the program's main menu.
Input: none.
Output: user's choice.
*/
int Menu::mainMenu()
{
	int choice = 0;

	do // Loop as long as choice is invalid
	{
		system("CLS"); // Clear the console
		cout << "Enter 0 to add a new shape." << endl
			 << "Enter 1 to modify or get information from a current shape." << endl
			 << "Enter 2 to delete all of the shapes." << endl
			 << "Enter 3 to exit." << endl << flush;

		cin >> choice; // Get user's choice
	} while (choice < ADD_NEW_SHAPE || choice > EXIT);

	return choice;
}

/*
Function prints the program's "add new shape" menu
and adds a new shape to the vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::addNewShape(vector<Shape*>& shapes)
{
	int choice = 0;

	do // Loop as long as choice is invalid
	{
		system("CLS"); // Clear the console
		cout << "Enter 0 to add a circle." << endl
			 << "Enter 1 to add an arrow." << endl
			 << "Enter 2 to add a triangle." << endl
			 << "Enter 3 to add a rectangle." << endl << flush;

		cin >> choice; // Get user's choice
	} while (choice < ADD_CIRCLE || choice > ADD_RECT);

	switch (choice)
	{
		case ADD_CIRCLE:
			this->addCircle(shapes);
			break;
		case ADD_ARROW:
			this->addArrow(shapes);
			break;
		case ADD_TRI:
			this->addTriangle(shapes);
			break;
		case ADD_RECT:
			this->addRectangle(shapes);
			break;
	}
}

/*
Function allows user to either move, remove or get the details
of a specific shape.
Input: shapes vector reference.
Output: none.
*/
void Menu::modifyGetInfo(vector<Shape*>& shapes)
{
	int shapeIndex = 0, choice = 0;
	double x = 0, y = 0;

	if (!shapes.empty()) // Only possible if the shapes vector isn't empty
	{
		shapeIndex = printShapes(shapes); // Print list of shapes and get shape choice from user.

		// Print menu and get choice
		do
		{
			cout << "Enter 0 to move the shape" << endl
				<< "Enter 1 to get its details." << endl
				<< "Enter 2 to remove the shape." << endl;

			cin >> choice;
		} while (choice < MOVE || choice > REMOVE); // Try again while input is invalid.

		switch (choice)
		{
		case MOVE:
		{
			system("CLS"); // Clear the screen

			// Get moving scale from user
			cout << "Please enter the X moving scale:" << endl << flush;
			cin >> x;
			cout << "Please enter the Y moving scale:" << endl << flush;
			cin >> y;

			Point movingScale(x, y); // Create moving scale point

			shapes[shapeIndex]->clearDraw(_canvas); // Clear the drawing of the shape
			shapes[shapeIndex]->move(movingScale); // Move the shape
			this->drawAll(shapes); // Draw all shapes again

			break;
		}
		case GET_DETAILS:

			// Print the shape's details
			shapes[shapeIndex]->printDetails();
			system("PAUSE");

			break;

		case REMOVE:

			shapes[shapeIndex]->clearDraw(_canvas); // Clear the drawing of the shape
			shapes.erase(shapes.begin() + shapeIndex); // Remove it from shapes vector
			this->drawAll(shapes); // Draw all shapes again

			break;
		}
	}
}

/*
Function deletes all shapes from the vector.
Input: shapes vector reference.
Output: none.
*/
void Menu::deleteAll(vector<Shape*>& shapes)
{
	clearAll(shapes); // Clear the canvas
	shapes.clear(); // Empty the vector
}

