#include "Menu.h"
#include "Polygon.h"
#include "Triangle.h"
#include "Canvas.h"

using std::vector;

int main(void)
{
	Menu menu;
	vector<Shape*> shapes;
	unsigned int choice = 0;
	
	choice = menu.mainMenu();

	while (choice != EXIT)
	{
		switch (choice)
		{
			case ADD_NEW_SHAPE:

				menu.addNewShape(shapes);
				break;

			case MODIFY:

				menu.modifyGetInfo(shapes);
				break;

			case DELETE_ALL:

				menu.deleteAll(shapes);
				break;
		}

		choice = menu.mainMenu();
	}

	return 0;
}